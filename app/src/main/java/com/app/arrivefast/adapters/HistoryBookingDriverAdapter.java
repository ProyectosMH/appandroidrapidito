package com.app.arrivefast.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.arrivefast.R;
import com.app.arrivefast.activities.driver.HistoryBookingDetailsDriverActivity;
import com.app.arrivefast.models.HistoryBooking;
import com.app.arrivefast.providers.ClientProvider;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class HistoryBookingDriverAdapter extends FirebaseRecyclerAdapter<HistoryBooking, HistoryBookingDriverAdapter.ViewHolder> {

    private ClientProvider mClientProvider;
    private Context mcontext;

    public HistoryBookingDriverAdapter(FirebaseRecyclerOptions<HistoryBooking> options, Context mcontext){
        super(options);
        mClientProvider = new ClientProvider();
        this.mcontext = mcontext;
    }

    @Override
    protected void onBindViewHolder(@NonNull final HistoryBookingDriverAdapter.ViewHolder holder, int position, @NonNull HistoryBooking historyBooking) {
        final String id = getRef(position).getKey();
        mClientProvider.getClient(historyBooking.getIdClient()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String nombre = snapshot.child("name").getValue().toString();
                    holder.tvNombre.setText(nombre);
                    if(snapshot.hasChild("image")){
                        String image = snapshot.child("image").getValue().toString();
                        Picasso.with(mcontext).load(image).into(holder.ivImage);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        holder.tvOrigen.setText(historyBooking.getOrigin());
        holder.tvDestino.setText(historyBooking.getDestination());
        holder.tvCalificacion.setText(String.valueOf(historyBooking.getCalificationDriver()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mcontext, HistoryBookingDetailsDriverActivity.class);
                intent.putExtra("idHistoryBooking", id);
                mcontext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public HistoryBookingDriverAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_history_booking, parent, false);
        return new HistoryBookingDriverAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNombre, tvOrigen, tvDestino, tvCalificacion;
        private ImageView ivImage;
        private View mView;

        public ViewHolder(View view){
            super(view);

            mView = view;
            tvNombre = view.findViewById(R.id.tvNombre);
            tvOrigen = view.findViewById(R.id.tvOrigen);
            tvDestino = view.findViewById(R.id.tvDestino);
            tvCalificacion = view.findViewById(R.id.tvCalificacion);
            ivImage = view.findViewById(R.id.ivImage);
        }
    }
}
