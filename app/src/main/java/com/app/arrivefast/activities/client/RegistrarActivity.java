package com.app.arrivefast.activities.client;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.app.arrivefast.R;
import com.app.arrivefast.activities.SelectOptionAuthActivity;
import com.app.arrivefast.activities.driver.RegistrarDriverActivity;
import com.app.arrivefast.includes.MyToolbar;
import com.app.arrivefast.models.Client;
import com.app.arrivefast.providers.AuthProvider;
import com.app.arrivefast.providers.ClientProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import dmax.dialog.SpotsDialog;

public class RegistrarActivity extends AppCompatActivity {

    AuthProvider mAuthProvider;
    ClientProvider mClientProvider;
    private TextInputEditText tietNombre, tietEmail, tietPass;
    private Button btnRegistrar;
    private String nombre = "", email = "", pass = "";
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        dialog = new SpotsDialog.Builder().setContext(RegistrarActivity.this).setMessage("Espere un momento por favor..").build();
        MyToolbar.showToolbar(this, "Registrarme", true);
        mAuthProvider = new AuthProvider();
        mClientProvider = new ClientProvider();

        tietNombre = findViewById(R.id.tietNombre);
        tietEmail = findViewById(R.id.tietEmail);
        tietPass = findViewById(R.id.tietPass);
        btnRegistrar = findViewById(R.id.btnRegistrar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrar();
            }
        });
    }

    private void registrar() {
        nombre = tietNombre.getText().toString();
        email = tietEmail.getText().toString();
        pass = tietPass.getText().toString();
        if(!nombre.isEmpty() && !email.isEmpty() && !pass.isEmpty()){
            if(pass.length() >= 6){
                dialog.show();
                registrarUsuario(nombre, email, pass);
            } else{
                Toast.makeText(RegistrarActivity.this, "La contraseña debe tener como minimo 6 caracteres", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(RegistrarActivity.this, "nombre, email y contraseña son obligatios", Toast.LENGTH_LONG).show();
        }
    }

    private void registrarUsuario(final String nombre, final String email, String pass) {
        mAuthProvider.register(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    Client client = new Client(id, nombre, email);
                    create(client);
                } else {
                    dialog.dismiss();
                    Toast.makeText(RegistrarActivity.this, "El usuario no pudo ser registrado", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    void create(Client client){
        mClientProvider.create(client).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    //Toast.makeText(RegistrarActivity.this, "Usuario creado exitosamente", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RegistrarActivity.this, MapClientActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(RegistrarActivity.this, "El usuario no pudo ser registrado", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
    }

    /*private void saveUser(String id, String nombre, String email) {

        User usuario = new User();
        usuario.setNombre(nombre);
        usuario.setEmail(email);

        if(user.equals("driver")){
            mDatabase.child("Users").child("Drivers").child(id).setValue(usuario).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(RegistrarActivity.this, "Usuario creado exitosamente", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(RegistrarActivity.this, "El usuario no pudo ser registrado", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                }
            });
        }else if(user.equals("client")){
            mDatabase.child("Users").child("Clients").child(id).setValue(usuario).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(RegistrarActivity.this, "Usuario creado exitosamente", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(RegistrarActivity.this, "El usuario no pudo ser registrado", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                }
            });
        }
    }*/
}