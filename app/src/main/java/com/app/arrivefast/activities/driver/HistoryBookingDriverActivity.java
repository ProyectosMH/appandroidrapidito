package com.app.arrivefast.activities.driver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.app.arrivefast.R;
import com.app.arrivefast.adapters.HistoryBookingDriverAdapter;
import com.app.arrivefast.includes.MyToolbar;
import com.app.arrivefast.models.HistoryBooking;
import com.app.arrivefast.providers.AuthProvider;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class HistoryBookingDriverActivity extends AppCompatActivity {

    private RecyclerView rvHistoryDriver;
    private HistoryBookingDriverAdapter mAdapter;
    private AuthProvider mAuthProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_booking_driver);

        MyToolbar.showToolbar(HistoryBookingDriverActivity.this, "Historial de viajes", true);

        rvHistoryDriver = findViewById(R.id.rvHistoryDriver);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvHistoryDriver.setLayoutManager(linearLayoutManager);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mAuthProvider = new AuthProvider();
        Query query = FirebaseDatabase.getInstance().getReference()
                .child("HistoryBooking")
                .orderByChild("idDriver")
                .equalTo(mAuthProvider.getId());
        FirebaseRecyclerOptions<HistoryBooking> options = new FirebaseRecyclerOptions.Builder<HistoryBooking>()
                .setQuery(query, HistoryBooking.class).build();
        mAdapter = new HistoryBookingDriverAdapter(options, HistoryBookingDriverActivity.this);
        rvHistoryDriver.setAdapter(mAdapter);
        mAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }
}