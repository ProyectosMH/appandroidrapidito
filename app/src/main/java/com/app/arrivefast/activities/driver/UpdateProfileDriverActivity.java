package com.app.arrivefast.activities.driver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.arrivefast.R;
import com.app.arrivefast.includes.MyToolbar;
import com.app.arrivefast.models.Client;
import com.app.arrivefast.models.Driver;
import com.app.arrivefast.providers.AuthProvider;
import com.app.arrivefast.providers.DriverProvider;
import com.app.arrivefast.providers.ImagesProvider;
import com.app.arrivefast.utils.CompressorBitmapImage;
import com.app.arrivefast.utils.FileUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class UpdateProfileDriverActivity extends AppCompatActivity {

    private ImageView ivProfile;
    private TextView tietNombre, tietMarca, tietPlaca;
    private Button btnActualizar;
    private DriverProvider mDriverProvider;
    private AuthProvider mAuthProvider;
    private ImagesProvider mImagesProvider;
    private File mFileImage;
    private String mImageUrl;
    private final int GALLERY_REQUEST = 1;
    private ProgressDialog mProgressDialog;
    private String mName, mMarca, mPlaca;
    private CircleImageView civBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile_driver);

        //MyToolbar.showToolbar(UpdateProfileDriverActivity.this, "Actualizar perfil", true);
        mProgressDialog = new ProgressDialog(this);
        mDriverProvider = new DriverProvider();
        mAuthProvider = new AuthProvider();
        mImagesProvider = new ImagesProvider("driver_images");

        ivProfile = findViewById(R.id.ivProfile);
        tietNombre = findViewById(R.id.tietNombre);
        tietMarca = findViewById(R.id.tietMarca);
        tietPlaca = findViewById(R.id.tietPlaca);
        civBack = findViewById(R.id.civBack);
        btnActualizar = findViewById(R.id.btnActualizar);

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });

        civBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getDriverInfo();
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mFileImage = FileUtil.from(this, data.getData());
                ivProfile.setImageBitmap(BitmapFactory.decodeFile(mFileImage.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error", "Error: " + e.getMessage());
            }
        }
    }

    private void getDriverInfo(){
        mDriverProvider.getDriver(mAuthProvider.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String name = snapshot.child("nombre").getValue().toString();
                    String marca = snapshot.child("marca").getValue().toString();
                    String placa = snapshot.child("placa").getValue().toString();
                    String image = "";
                    if (snapshot.hasChild("image")) {
                        image = snapshot.child("image").getValue().toString();
                        Picasso.with(UpdateProfileDriverActivity.this).load(image).into(ivProfile);
                    }
                    tietNombre.setText(name);
                    tietMarca.setText(marca);
                    tietPlaca.setText(placa);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void updateProfile() {
        mName = tietNombre.getText().toString();
        mMarca = tietMarca.getText().toString();
        mPlaca = tietPlaca.getText().toString();
        if (!mName.equals("") && !mMarca.equals("") && !mPlaca.equals("") && mFileImage != null) {
            mProgressDialog.setMessage("Espere un momento...");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();

            saveImage();
        }
        else {
            Toast.makeText(this, "Ingresa la imagen, el nombre, la marca y la placa", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveImage() {
        mImagesProvider.saveImages(UpdateProfileDriverActivity.this, mFileImage, mAuthProvider.getId()).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    mImagesProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String image = uri.toString();
                            Driver driver = new Driver();
                            driver.setImage(image);
                            driver.setNombre(mName);
                            driver.setMarca(mMarca);
                            driver.setPlaca(mPlaca);
                            driver.setId(mAuthProvider.getId());
                            mDriverProvider.update(driver).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mProgressDialog.dismiss();
                                    Toast.makeText(UpdateProfileDriverActivity.this, "Su informacion se actualizo correctamente", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
                else {
                    Toast.makeText(UpdateProfileDriverActivity.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}