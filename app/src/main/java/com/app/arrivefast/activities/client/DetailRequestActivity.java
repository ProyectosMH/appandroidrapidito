package com.app.arrivefast.activities.client;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.arrivefast.R;
import com.app.arrivefast.includes.MyToolbar;
import com.app.arrivefast.models.Info;
import com.app.arrivefast.providers.GoogleApiProvider;
import com.app.arrivefast.providers.InfoProvider;
import com.app.arrivefast.utils.DecodePoints;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailRequestActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private double mExtraOriginLat, mExtraOriginLon, mExtraDestinationLat, mExtraDestinationLon;
    private LatLng mOrigenLatLng, mDestinationLatLng;
    private GoogleApiProvider mGoogleApiProvider;
    private List<LatLng> mPolyLineList;
    private PolylineOptions mPolylineOptions;
    private TextView tvOrigen, tvDestino, tvTime, tvPrice;
    private String mExtraOrigen = "", mExtraDestino = "";
    private Button btnRequestNow;
    private CircleImageView civBack;
    private InfoProvider mInfoProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_request);

        //MyToolbar.showToolbar(this, "Solicitar viaje", true);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        tvOrigen = findViewById(R.id.tvOrigen);
        tvDestino = findViewById(R.id.tvDestino);
        tvTime = findViewById(R.id.tvTime);
        tvPrice = findViewById(R.id.tvPrice);
        btnRequestNow = findViewById(R.id.btnRequestNow);
        civBack = findViewById(R.id.civBack);

        mExtraOriginLat = getIntent().getDoubleExtra("origin_lat", 0);
        mExtraOriginLon = getIntent().getDoubleExtra("origin_lon", 0);
        mExtraDestinationLat = getIntent().getDoubleExtra("destination_lat", 0);
        mExtraDestinationLon = getIntent().getDoubleExtra("destination_lon", 0);
        mGoogleApiProvider = new GoogleApiProvider(DetailRequestActivity.this);
        mInfoProvider = new InfoProvider();
        mExtraOrigen = getIntent().getStringExtra("origen");
        mExtraDestino = getIntent().getStringExtra("destino");

        tvOrigen.setText(mExtraOrigen);
        tvDestino.setText(mExtraDestino);

        mOrigenLatLng = new LatLng(mExtraOriginLat, mExtraOriginLon);
        mDestinationLatLng = new LatLng(mExtraDestinationLat, mExtraDestinationLon);

        btnRequestNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRequestDriver();
            }
        });

        civBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void goToRequestDriver() {
        Intent intent = new Intent(DetailRequestActivity.this, RequestDriverActivity.class);
        intent.putExtra("origin_lon", mOrigenLatLng.longitude);
        intent.putExtra("origin_lat", mOrigenLatLng.latitude);
        intent.putExtra("origin", mExtraOrigen);
        intent.putExtra("destination", mExtraDestino);
        intent.putExtra("destination_lat", mDestinationLatLng.latitude);
        intent.putExtra("destination_lon", mDestinationLatLng.longitude);
        startActivity(intent);
        finish();
    }

    private void drawRoud(){
        mGoogleApiProvider.getDirections(mOrigenLatLng, mDestinationLatLng).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    JSONArray jsonArray = jsonObject.getJSONArray("routes");
                    JSONObject routes = jsonArray.getJSONObject(0);
                    JSONObject polyLines = routes.getJSONObject("overview_polyline");
                    String points = polyLines.getString("points");
                    mPolyLineList = DecodePoints.decodePoly(points);
                    mPolylineOptions = new PolylineOptions();
                    mPolylineOptions.color(Color.DKGRAY);
                    mPolylineOptions.width(13f);
                    mPolylineOptions.startCap(new SquareCap());
                    mPolylineOptions.jointType(JointType.ROUND);
                    mPolylineOptions.addAll(mPolyLineList);
                    mMap.addPolyline(mPolylineOptions);

                    JSONArray legs = routes.getJSONArray("legs");
                    JSONObject leg = legs.getJSONObject(0);
                    JSONObject distance = leg.getJSONObject("distance");
                    JSONObject duration = leg.getJSONObject("duration");
                    String distanceText = distance.getString("text");
                    String durationText = duration.getString("text");
                    tvTime.setText(durationText + " " + distanceText);

                    String[] distanceAndKm = distanceText.split(" ");
                    double distanceValue = Double.parseDouble(distanceAndKm[0]);
                    String[] timeAndMin = distanceText.split(" ");
                    double timeValue = Double.parseDouble(timeAndMin[0]);
                    
                    calcularPrecio(distanceValue, timeValue);
                }catch (Exception e){
                    Log.d("Error", "Erro: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void calcularPrecio(final double distanceValue, final double timeValue) {
        mInfoProvider.getInfo().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    Info info = snapshot.getValue(Info.class);
                    double totalKm = distanceValue * info.getKm();
                    double totalMin = timeValue * info.getMin();
                    double total = totalKm + totalMin;
                    tvPrice.setText("$" + total);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.addMarker(new MarkerOptions().position(mOrigenLatLng).title("Origen").icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map_pin_red)));
        mMap.addMarker(new MarkerOptions().position(mDestinationLatLng).title("Destino").icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map_pin_blue)));
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(mOrigenLatLng)
                        .zoom(14f)
                        .build()
        ));

        drawRoud();
    }
}