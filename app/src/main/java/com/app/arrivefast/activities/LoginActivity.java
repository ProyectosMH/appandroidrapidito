package com.app.arrivefast.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.app.arrivefast.R;
import com.app.arrivefast.activities.client.MapClientActivity;
import com.app.arrivefast.activities.client.RegistrarActivity;
import com.app.arrivefast.activities.driver.MapDriverActivity;
import com.app.arrivefast.includes.MyToolbar;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences mpreferen;
    private TextInputEditText tiveMail, tivePass;
    private Button btnLogin;
    private String email = "", pass = "";
    private CircleImageView civBack;

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tiveMail = findViewById(R.id.tiveMail);
        tivePass = findViewById(R.id.tivePass);
        btnLogin = findViewById(R.id.btnLogin);
        civBack = findViewById(R.id.civBack);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        dialog = new SpotsDialog.Builder().setContext(LoginActivity.this).setMessage("Espere un momento por favor..").build();
        mpreferen = getApplicationContext().getSharedPreferences("typeUser", MODE_PRIVATE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        civBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void login() {
        email = tiveMail.getText().toString();
        pass = tivePass.getText().toString();
        if(!email.equals("") && !pass.equals("")){
            if(pass.length() >= 6){
                dialog.show();
                mAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            String tipo = mpreferen.getString("user", "");
                            if(tipo.equals("client")){
                                Intent intent = new Intent(LoginActivity.this, MapClientActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else{
                                Intent intent = new Intent(LoginActivity.this, MapDriverActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                            //Toast.makeText(LoginActivity.this, "Login se realizo exitosamente", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(LoginActivity.this, "La contraseña o email son incorrectos", Toast.LENGTH_LONG).show();
                        }
                        dialog.dismiss();
                    }
                });
            }else{
                Toast.makeText(LoginActivity.this, "La contraseña debe tener como minimo 6 caracteres", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(LoginActivity.this, "Email y contraseña son obligatios", Toast.LENGTH_LONG).show();
        }
    }
}