package com.app.arrivefast.activities.client;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.arrivefast.R;
import com.app.arrivefast.models.HistoryBooking;
import com.app.arrivefast.providers.DriverProvider;
import com.app.arrivefast.providers.HistoryBookingProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class HistoryBookingDetailsClientActivity extends AppCompatActivity {

    private TextView tvNombre, tvOrigen, tvDestino, tvCalificacion;
    private CircleImageView civImage, civBack;
    private RatingBar rbCalificacion;
    private String mExtraId = "";
    private HistoryBookingProvider mHistoryBookingProvider;
    private DriverProvider mDriverProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_booking_details_client);

        mHistoryBookingProvider = new HistoryBookingProvider();
        mDriverProvider = new DriverProvider();

        tvNombre = findViewById(R.id.tvNombre);
        tvOrigen = findViewById(R.id.tvOrigen);
        tvDestino = findViewById(R.id.tvDestino);
        tvCalificacion = findViewById(R.id.tvCalificacion);
        civImage = findViewById(R.id.civImage);
        civBack = findViewById(R.id.civBack);
        rbCalificacion = findViewById(R.id.rbCalificacion);

        civBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mExtraId = getIntent().getStringExtra("idHistoryBooking");
        getHistoryBooking();
    }

    private void getHistoryBooking() {
        mHistoryBookingProvider.getHistoryBooking(mExtraId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    HistoryBooking historyBooking = snapshot.getValue(HistoryBooking.class);

                    mDriverProvider.getDriver(historyBooking.getIdDriver()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){
                                String nombre = snapshot.child("nombre").getValue().toString();
                                tvNombre.setText(nombre.toUpperCase());
                                if(snapshot.hasChild("image")){
                                    String image = snapshot.child("image").getValue().toString();
                                    Picasso.with(HistoryBookingDetailsClientActivity.this).load(image).into(civImage);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                    tvOrigen.setText(historyBooking.getOrigin());
                    tvDestino.setText(historyBooking.getDestination());
                    tvCalificacion.setText("Tu calificacion fue de: " + historyBooking.getCalificationDriver());
                    if(snapshot.hasChild("calificationClient")){
                        rbCalificacion.setRating((float) historyBooking.getCalificationClient());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}