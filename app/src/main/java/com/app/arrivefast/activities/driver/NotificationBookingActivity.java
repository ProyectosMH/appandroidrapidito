package com.app.arrivefast.activities.driver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.arrivefast.R;
import com.app.arrivefast.providers.AuthProvider;
import com.app.arrivefast.providers.ClientBookingProvider;
import com.app.arrivefast.providers.GeofireProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class NotificationBookingActivity extends AppCompatActivity {

    private TextView tvDesde, tvHasta, tvTiempoLlegada, tvDistancia, tvCounter;
    private Button btnAceptarBooking, btnCancelBooking;
    private ClientBookingProvider mClientBookingProvider;
    private GeofireProvider mGeofireProvider;
    private AuthProvider mAuthProvider;
    private MediaPlayer mMediaPlayer;
    private String mExtraIdClient, mExtraOrigin, mExtraDestination, mExtraTime, mExtraDistance;
    private Handler mHandler;
    private int mCounter = 10;
    private ValueEventListener mListener;

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mCounter = mCounter - 1;
            tvCounter.setText(String.valueOf(mCounter));
            if(mCounter > 0){
                initTimer();
            }else{
                cancelBooking();
            }
        }
    };

    private void initTimer(){
        mHandler = new Handler();
        mHandler.postDelayed(runnable, 1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_booking);

        tvDesde = findViewById(R.id.tvDesde);
        tvHasta = findViewById(R.id.tvHasta);
        tvTiempoLlegada = findViewById(R.id.tvTiempoLlegada);
        tvDistancia = findViewById(R.id.tvDistancia);
        tvCounter = findViewById(R.id.tvCounter);
        btnAceptarBooking = findViewById(R.id.btnAceptarBooking);
        btnCancelBooking = findViewById(R.id.btnCancelBooking);
        mMediaPlayer = MediaPlayer.create(this, R.raw.next_episode_marimba);
        mMediaPlayer.setLooping(true);
        mClientBookingProvider = new ClientBookingProvider();

        mExtraIdClient = getIntent().getStringExtra("idClient");
        mExtraOrigin = getIntent().getStringExtra("origin");
        mExtraDestination = getIntent().getStringExtra("destination");
        mExtraTime = getIntent().getStringExtra("time");
        mExtraDistance = getIntent().getStringExtra("distance");

        tvDesde.setText(mExtraOrigin);
        tvHasta.setText(mExtraDestination);
        tvTiempoLlegada.setText(mExtraTime);
        tvDistancia.setText(mExtraDistance);

        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        );

        btnAceptarBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptBookin();
            }
        });

        btnCancelBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelBooking();
            }
        });

        initTimer();
        checkIfClientCancelBooking();
    }

    private void acceptBookin() {
        if(mHandler != null) mHandler.removeCallbacks(runnable);

        mAuthProvider = new AuthProvider();
        mGeofireProvider = new GeofireProvider("active_drivers");
        mGeofireProvider.removeLocation(mAuthProvider.getId());

        mClientBookingProvider = new ClientBookingProvider();
        mClientBookingProvider.updateStatus(mExtraIdClient, "accept");

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(2);

        Intent intent1 = new Intent(getApplicationContext(), MapDriverBookingActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent1.setAction(Intent.ACTION_RUN);
        intent1.putExtra("idClient", mExtraIdClient);
        startActivity(intent1);
    }

    private void cancelBooking() {
        if(mHandler != null) mHandler.removeCallbacks(runnable);

        mClientBookingProvider.updateStatus(mExtraIdClient, "cancel");

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(2);

        Intent intent = new Intent(NotificationBookingActivity.this, MapDriverActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkIfClientCancelBooking() {
        mListener = mClientBookingProvider.getClientBooking(mExtraIdClient).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(NotificationBookingActivity.this, "El cliente cancelo el viaje", Toast.LENGTH_LONG).show();
                    if (mHandler != null) mHandler.removeCallbacks(runnable);
                    Intent intent = new Intent(NotificationBookingActivity.this, MapDriverActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.release();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMediaPlayer != null) {
            if (!mMediaPlayer.isPlaying()) {
                mMediaPlayer.start();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) mHandler.removeCallbacks(runnable);

        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }
        if (mListener != null) {
            mClientBookingProvider.getClientBooking(mExtraIdClient).removeEventListener(mListener);
        }
    }
}