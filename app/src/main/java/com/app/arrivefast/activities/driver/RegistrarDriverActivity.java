package com.app.arrivefast.activities.driver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.app.arrivefast.R;
import com.app.arrivefast.activities.client.MapClientActivity;
import com.app.arrivefast.activities.client.RegistrarActivity;
import com.app.arrivefast.includes.MyToolbar;
import com.app.arrivefast.models.Driver;
import com.app.arrivefast.providers.AuthProvider;
import com.app.arrivefast.providers.DriverProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import dmax.dialog.SpotsDialog;

public class RegistrarDriverActivity extends AppCompatActivity {

    AuthProvider mAuthProvider;
    DriverProvider mDriverProvider;
    private TextInputEditText tietNombre, tietEmail, tietPass, tietMarca, tietPlaca;
    private Button btnRegistrar;
    private String nombre = "", email = "", pass = "", marca = "", placa = "";
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_driver);

        dialog = new SpotsDialog.Builder().setContext(RegistrarDriverActivity.this).setMessage("Espere un momento por favor..").build();
        MyToolbar.showToolbar(this, "Registrarme", true);
        mAuthProvider = new AuthProvider();
        mDriverProvider = new DriverProvider();

        tietNombre = findViewById(R.id.tietNombre);
        tietEmail = findViewById(R.id.tietEmail);
        tietMarca = findViewById(R.id.tietMarca);
        tietPlaca = findViewById(R.id.tietPlaca);
        tietPass = findViewById(R.id.tietPass);
        btnRegistrar = findViewById(R.id.btnRegistrar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrar();
            }
        });
    }

    private void registrar() {
        nombre = tietNombre.getText().toString();
        email = tietEmail.getText().toString();
        pass = tietPass.getText().toString();
        marca = tietMarca.getText().toString();
        placa = tietPlaca.getText().toString();
        if(!nombre.isEmpty() && !email.isEmpty() && !pass.isEmpty() && !marca.isEmpty() && !placa.isEmpty()){
            if(pass.length() >= 6){
                dialog.show();
                registrarUsuario(nombre, email, pass, marca, placa);
            } else{
                Toast.makeText(RegistrarDriverActivity.this, "La contraseña debe tener como minimo 6 caracteres", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(RegistrarDriverActivity.this, "nombre, email y contraseña son obligatios", Toast.LENGTH_LONG).show();
        }
    }

    private void registrarUsuario(final String nombre, final String email, String pass, final String marca, final String placa) {
        mAuthProvider.register(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    Driver driver = new Driver(id, nombre, email, marca, placa);
                    create(driver);
                } else {
                    dialog.dismiss();
                    Toast.makeText(RegistrarDriverActivity.this, "El usuario no pudo ser registrado", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    void create(Driver driver){
        mDriverProvider.create(driver).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    //Toast.makeText(RegistrarDriverActivity.this, "Usuario creado exitosamente", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RegistrarDriverActivity.this, MapDriverActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(RegistrarDriverActivity.this, "El usuario no pudo ser registrado", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
    }
}