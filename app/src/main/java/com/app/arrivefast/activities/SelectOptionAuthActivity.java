package com.app.arrivefast.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.app.arrivefast.R;
import com.app.arrivefast.activities.client.RegistrarActivity;
import com.app.arrivefast.activities.driver.RegistrarDriverActivity;
import com.app.arrivefast.includes.MyToolbar;

public class SelectOptionAuthActivity extends AppCompatActivity {

    SharedPreferences mpreferen;
    private Button btnLogin, btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_option_auth);

        MyToolbar.showToolbar(this, "Opciones", true);
        mpreferen = getApplicationContext().getSharedPreferences("typeUser", MODE_PRIVATE);

        btnLogin = findViewById(R.id.btnLogin);
        btnRegistrar = findViewById(R.id.btnRegistrar);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLogin();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegistrar();
            }
        });
    }

    private void goToRegistrar() {
        String tipo = mpreferen.getString("user", "");
        if(tipo.equals("client")){
            Intent intent = new Intent(SelectOptionAuthActivity.this, RegistrarActivity.class);
            startActivity(intent);
        }else{
            Intent intent = new Intent(SelectOptionAuthActivity.this, RegistrarDriverActivity.class);
            startActivity(intent);
        }
    }

    private void goToLogin() {
        Intent intent = new Intent(SelectOptionAuthActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}