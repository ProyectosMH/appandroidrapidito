package com.app.arrivefast.models;

public class Driver {
    String id;
    String nombre;
    String email;
    String marca;
    String placa;
    String image;

    public Driver(){
    }

    public Driver(String id, String nombre, String email, String marca, String placa) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.marca = marca;
        this.placa = placa;
    }

    public Driver(String id, String nombre, String email, String marca, String placa, String image) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.marca = marca;
        this.placa = placa;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
