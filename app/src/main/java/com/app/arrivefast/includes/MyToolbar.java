package com.app.arrivefast.includes;

import androidx.appcompat.app.AppCompatActivity;

import com.app.arrivefast.R;

public class MyToolbar {

    public static void showToolbar(AppCompatActivity activity, String title, boolean buttonBack){
        androidx.appcompat.widget.Toolbar tbToolbar = activity.findViewById(R.id.tbToolbar);
        activity.setSupportActionBar(tbToolbar);
        activity.getSupportActionBar().setTitle(title);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(buttonBack);
    }
}
