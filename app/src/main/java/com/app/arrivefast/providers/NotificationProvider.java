package com.app.arrivefast.providers;

import com.app.arrivefast.models.FCMBody;
import com.app.arrivefast.models.FCMResponse;
import com.app.arrivefast.retrofit.IFCMApi;
import com.app.arrivefast.retrofit.RetrofitClient;

import retrofit2.Call;

public class NotificationProvider {

    private String url = "https://fcm.googleapis.com";

    public NotificationProvider() {
    }

    public Call<FCMResponse> sendNotification(FCMBody body){
        return RetrofitClient.getClientObject(url).create(IFCMApi.class).send(body);
    }
}
