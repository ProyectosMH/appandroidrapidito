package com.app.arrivefast.retrofit;

import com.app.arrivefast.models.FCMBody;
import com.app.arrivefast.models.FCMResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMApi {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAz_St-X8:APA91bG4y8_oMpGBD2TNJX3bGfGKI-tPxjg1983g-e7ZagrRVtkWAYvdnQP19V-9l8o0uYO9oJBBY8d4XIkPwzSIMbOF1_7iPxunwWbJHAlTvz9og9E4XBNkVz2efNydFgkviEsppgoF"
    })
    @POST("fcm/send")
    Call<FCMResponse> send(@Body FCMBody body);
}
